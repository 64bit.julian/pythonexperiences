#!/usr/bin/python
"""
    AWRg.py - Oracle AWR Generator 
    Copyright 2018 Alexandre Marti. All rights reserved. To more info send mail to alexandremarti@gmail.com.
    Licensed under the Apache License, Version 2.0. See LICENSE.txt for terms & conditions.

    Purpose: Generate AWR reports using datetime 
    Author: Alexandre Marti
                  
    Usage:       
        python AWRg.py -cs <oracle db url> -sd <start datetime> -ed <end datetime end> 

    Optional Parameters
        -in : instance number. the default value is 1
        -gb : if specified will generate only the GLOBAL awr report instead of multiples reports for each snap. Default is FALSE
        -rt : report type output format ('text' or 'html'). HTML is the default format
        -od : output directory when AWR will be generated. by default will be generated in the current location
        -un : to use a username other than system
        -pw : to send the password instead of informing when asked by the program
        -df : to defined no default mask used by to_date() oracle function (ex: -df 'yyyy/mm/dd hh24')
        -vb : verbose mode

    Example:
        python AWRg.py -cs 127.0.0.1:1521/orclcdb -sd "2018-12-10 22:00:35" -ed "2018-12-11 22:00:35" 

    Help:
        python AWRg.py -h

"""
import os
import sys
import argparse
import cx_Oracle
import getpass

def main():

    try:
        # CMD Parameters
        version = "1.1"
        version_description = "AWRg - Oracle AWR Generator {}".format(version)

        parser = argparse.ArgumentParser(description = version_description) 
        parser.add_argument("-vb", help="show execution information", action="store_true")
        parser.add_argument("-rt", help="html or text report", default = "html", choices = ['text', 'html'])
        parser.add_argument("-df", help="to_date() mask like dd/mm/yyyy hh24:mi:ss", default = "yyyy-mm-dd hh24:mi:ss")
        parser.add_argument("-sd", help="datetime to find initial oracle snap_id (format is defined by --date-format/-f parameter)")
        parser.add_argument("-ed", help="datetime to find final oracle snap_id, obvisiously need to be greater than --start-date (format is defined by --date_format/-f parameter)")
        parser.add_argument("-ni", help="number of instance (inst_id)", default=1, type=int)
        parser.add_argument("-cs", help="oracle url connect string. Ex: host:port/servicename, 127.0.0.1:1521/orclcdb", required=True)
        parser.add_argument("-un", help="oracle database username", default="system")
        parser.add_argument("-pw", help="username password. if empty will be prompted")
        parser.add_argument("-gb", help="if specified will generate only one global awr report", action="store_true")
        parser.add_argument("-od", help="path to write awr files. default is the location where the script is started", default="")
		
        parser.add_argument("-si", help="number of initial snap_id", default=0, type=int )
        parser.add_argument("-se", help="number of final snap_id", default=0, type=int)
        parser.add_argument("-rc", help="if specified will generate rac report", action="store_true")
		
            
        # If parse fail will show help
        args = parser.parse_args()

        # Parameters to generate AWR REPORT
        out_dir      = args.od
        r_type       = args.rt
        date_format  = args.df
        b_interval   = args.sd
        e_interval   = args.ed
        ora_instance = args.ni

        ora_user     = args.un    
        ora_str      = args.cs
        is_global    = args.gb
        verbose      = args.vb
        is_rac       = args.rc

        		
        if (args.sd is None) and (args.si == 0):    
            print("\n\n***\nPlease specify -sd or -si parameter\n")
            sys.exit(1)

        if (args.ed is None) and (args.se == 0):    
            print("\n\n***\nPlease specify -ed or -se parameter\n")
            sys.exit(1)
		    
        if args.pw is None:        
            ora_passwd = getpass.getpass('{} password: '.format(ora_user))
        else:
            ora_passwd = args.pw
        
        msg = lambda m : verbose and print(m)    
        
        try:    
            msg("Conecting to database {}".format(ora_str))
            ora_con = cx_Oracle.connect("{}/{}@{}".format(ora_user, ora_passwd, ora_str))
        except cx_Oracle.DatabaseError as e:
            error, = e.args
            print("\n\n***\nDatabase connection has been failed: \n   {}\nUsed connection string:\n   {}/****@{}\n\n".format(error.message, ora_user,ora_str))
            sys.exit(error.code)
        msg("Done.")  
    
        msg("Getting database informations")
        ora_cursor = ora_con.cursor()    
        ora_cursor.execute ("select dbid,name from v$database")
        ora_dbid, ora_dbname = ora_cursor.fetchone ()      
        msg("Done.")  
    
        msg("Getting start snap id number")
		
        if args.si == 0:
            ora_cursor.execute ("select max(snap_id) from dba_hist_snapshot where END_INTERVAL_TIME <= to_date(:1,:2)",[b_interval, date_format])
            ora_b_snap = ora_cursor.fetchone ()[0]
        else:
            ora_b_snap = args.si
		
        msg("Snap Id: {}".format(ora_b_snap))
        msg("Done.")  

        msg("Getting end snap id number")    
        if args.se == 0:
            ora_cursor.execute("select max(snap_id) from dba_hist_snapshot where END_INTERVAL_TIME <= to_date(:1,:2)",[e_interval, date_format])
            ora_e_snap = ora_cursor.fetchone ()[0]
        else:
            ora_e_snap = args.se

        msg("Snap Id: {}".format(ora_e_snap))
        msg("Done.")  

        if (ora_e_snap is None) or (ora_b_snap is None):    
            print("\n\n***\nFail to find oracle snaps in the interval. Check verbose messages to find the problem\n")
            if not verbose:
                print("Use -vb to enable verbose messages and run again")
        else:
            msg("Checking output format")
            if r_type=='text':
                sql = 'SELECT output FROM TABLE(dbms_workload_repository.awr_report_text (:1,:2,:3,:4))'
                sql_rac = 'SELECT output FROM TABLE(dbms_workload_repository.awr_global_report_text (:1,:2,:3,:4))'
                file_ext=".txt"
                msg("Output format is defined to text")
            else:
                sql = 'SELECT output FROM TABLE(dbms_workload_repository.awr_report_html (:1,:2,:3,:4))'
                sql_rac = 'SELECT output FROM TABLE(dbms_workload_repository.awr_global_report_html (:1,:2,:3,:4))'
                file_ext=".html"
                msg("Output format is defined to htmtl")

        
            file_name=out_dir +'awr_dbid_' + str(ora_dbid) +'_' + ora_dbname +'_inst_' + str(ora_instance) + '_' + str(ora_b_snap) + '_' + str(ora_e_snap) + file_ext 
            print("Generating GLOBAL AWR {}".format(file_name))
            ora_cursor.execute(sql,[ora_dbid, ora_instance, ora_b_snap, ora_e_snap])
            f = open(file_name,'w')
            for row in ora_cursor.fetchall():
                if row[0] is not None:
                    f.write('\n'+row[0])
            f.close
            msg("Done.")  

            if not is_global:
                msg("Specified to generate detailed awr reports")  
                for i in range(ora_b_snap,ora_e_snap):
                    file_name=out_dir +'awr_dbid_' + str(ora_dbid) +'_' + ora_dbname +'_inst_' + str(ora_instance) + '_' + str(i) + '_' + str(i+1) + file_ext
                    print("Generating AWR {}".format(file_name))
                    ora_cursor.execute(sql,[ora_dbid, ora_instance, i, i+1])
                    f = open(file_name,'w')
                    for row in ora_cursor.fetchall():
                        if row[0] is not None:
                            f.write('\n'+row[0])
                    f.close
                    msg("Done.") 
					
            if is_rac:
                msg("Specified to generate rac awr report")  
                
                file_name=out_dir +'awr_dbid_' + str(ora_dbid) +'_' + ora_dbname +'_rac_' + str(ora_b_snap) + '_' + str(ora_e_snap) + file_ext				
                print("Generating AWR {}".format(file_name))
                ora_cursor.execute(sql_rac,[ora_dbid, '', ora_b_snap, ora_e_snap])
                f = open(file_name,'w')
                for row in ora_cursor.fetchall():
                    if row[0] is not None:
                        f.write('\n'+row[0])
                f.close
                msg("Done.") 					

					
    finally:    
        print("\nAWRg.py Finished\n")


if __name__ == '__main__':
    main()
