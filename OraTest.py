#!/usr/bin/python
"""
    OraTest.py - Oracle Logon Test 
    Copyright 2018 Alexandre Marti. All rights reserved. To more info send mail to alexandremarti@gmail.com.
    Licensed under the Apache License, Version 2.0. See LICENSE.txt for terms & conditions.

    Purpose: Test the connection with oracle database 
    Author: Alexandre Marti
                  
    Usage:       
        python OraTest.py -cs <oracle db url> -vb

    Optional Parameters
        -un : to use a username other than system
        -pw : to send the password instead of informing when asked by the program        
        -vb : verbose mode
    Example:
        python OraTest.py -cs 127.0.0.1:1521/orclcdb  

    Help:
        python OraTest.py -h

"""
import os
import sys
import argparse
import cx_Oracle
import getpass

def main():

    try:
        # CMD Parameters
        version = "1.1"
        version_description = "OraTest - Oracle Logon Test {}".format(version)

        parser = argparse.ArgumentParser(description = version_description) 
        parser.add_argument("-vb", help="show execution information", action="store_true")
        parser.add_argument("-cs", help="oracle url connect string. Ex: host:port/servicename, 127.0.0.1:1521/orclcdb", required=True)
        parser.add_argument("-un", help="oracle database username", default="system")
        parser.add_argument("-pw", help="username password. if empty will be prompted")
            
        # If parse fail will show help
        args = parser.parse_args()

        ora_user     = args.un    
        ora_str      = args.cs        
        verbose      = args.vb

        if args.pw is None:        
            ora_passwd = getpass.getpass('{} password: '.format(ora_user))
        else:
            ora_passwd = args.pw
        
        msg = lambda m : verbose and print(m)    
        
        try:    
            msg("Conecting to database {}".format(ora_str))
            ora_con = cx_Oracle.connect("{}/{}@{}".format(ora_user, ora_passwd, ora_str))

            msg("Getting database informations")
            ora_cursor = ora_con.cursor()    
            ora_cursor.execute ("select dbid,name from v$database")
            ora_dbid, ora_dbname = ora_cursor.fetchone ()      
            msg("Done.")      
		
            print("User {} connected on {}. The database name is {}".format(ora_user,ora_str,ora_dbname))

        except cx_Oracle.DatabaseError as e:
            error, = e.args
            print("\n\n***\nDatabase connection has been failed: \n   {}\nUsed connection string:\n   {}/****@{}\n\n".format(error.message, ora_user,ora_str))
            sys.exit(error.code)
            msg("Done.")  
    
        
    finally:    
        print("\nOraTest.py Finished\n")


if __name__ == '__main__':
    main()
    